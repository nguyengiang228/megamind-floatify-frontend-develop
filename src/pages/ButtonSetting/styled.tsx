import styled from "styled-components";

export const ButtonSettingsStyled = styled.div<{
  $BgColor?: string;
  $Color?: string;
  $RangeValue: number;
}>`
  .category-buttons {
    display: flex;
    font-size: 1rem;
  }

  .category-item {
    height: 50px;
    padding: 0.5rem 1.6rem;
    color: rgba(109, 113, 117, 1);
    cursor: pointer;
    &:hover {
      color: black;
      border-bottom: 2px solid #dbdbdb;
    }
  }

  .cta-icon-buttons {
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .cta-icon {
    width: 30px;
    padding: 0 1px;
  }

  .cta-icon-hover {
    display: none;
    padding-right: 5px;
  }

  .cta-icon-default {
    display: flex;
    justify-content: center;
    align-items: center;
    min-width: 32px;
    height: 32px;
    cursor: pointer;
    border: 1px solid #dbdbdb;
    border-radius: 8px;
    background-color: ${({ $BgColor }) => $BgColor};
    svg {
      fill: ${({ $Color }) => $Color};
    }
    opacity: ${({ $RangeValue }) => $RangeValue * 0.01};
    &:hover .cta-icon-hover {
      display: block;
    }
  }

  .cta-icon-type {
    display: flex;
    margin: 20px 0;
    align-items: center;
  }

  .cta-icon-margin {
    display: flex;
    margin: 0 5em;
  }

  .cta-icon-button {
    width: 6rem;
    padding: 0 10px;
  }

  .button-change {
    width: 8em;
    border: 1px solid #dbdbdb;
    border-radius: 8px;
    margin-top: 10px;
  }

  .button-upload-file {
    width: 10rem;
    height: auto;
  }

  .cta-input-color {
    display: flex;
    margin: 0 20px;
    border: none;
  }

  .active {
    border-bottom: 3px solid #008060;
  }

  .text-field {
    margin: 0 20px;
    border: "none";
    width: 20em;
  }
  .text-field-inline {
    margin: 0 15px;
    border: "none";
    width: 20em;
  }

  .text-field-checkbox {
    display: flex;
    align-items: center;
  }
`;
