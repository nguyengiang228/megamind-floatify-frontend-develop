import {
  BlockStack,
  Card,
  InlineGrid,
  InlineStack,
  Layout,
  Page,
} from "@shopify/polaris";
import { ButtonSettingsStyled } from "./styled";
import { useState } from "react";
import RenderTab from "./components";
import { TabEnum } from "~/constants/enum";
import { buttonsArr } from "./config";

const ButtonSettings = () => {
  const [keyEnum, setkeyEnum] = useState<string>(TabEnum.CTA_BUTTON);

  const handleChangeTab = (item: string) => {
    setkeyEnum(item);
  };

  return (
    <ButtonSettingsStyled $RangeValue={0}>
      <Page
        title="Button Settings"
        backAction={{ content: "GroupButton", url: "/group-button" }}
      >
        <Layout>
          <Layout.Section variant="oneHalf">
            <Card>
              <InlineGrid columns="1fr auto">
                <InlineStack blockAlign="center" gap="200">
                  {buttonsArr.map((item, index) => (
                    <div
                      key={index}
                      onClick={() => handleChangeTab(item.key)}
                      className={
                        item.key === keyEnum
                          ? "category-item active"
                          : "category-item "
                      }
                    >
                      <div>{item.label}</div>
                    </div>
                  ))}
                </InlineStack>
              </InlineGrid>
            </Card>
          </Layout.Section>
          <Layout.Section>
            <BlockStack gap="400">
              <RenderTab checkedTab={keyEnum} />
            </BlockStack>
          </Layout.Section>
        </Layout>
      </Page>
    </ButtonSettingsStyled>
  );
};

export default ButtonSettings;
