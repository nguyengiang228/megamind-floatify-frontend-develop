import { TabEnum } from "~/constants/enum";

export const buttonsArr = [
  { label: "CTA buttons", key: TabEnum.CTA_BUTTON },
  { label: "Social buttons", key: TabEnum.SOCIAL_BUTTON },
  { label: "Share buttons", key: TabEnum.SHARE_BUTTON },
];
