import CustomText from "~/components/CustomText/CustomText";
import { ButtonSettingsStyled } from "../styled";
import {
  Button,
  Card,
  Divider,
  DropZone,
  Grid,
  RadioButton,
  RangeSlider,
  TextField,
  Thumbnail,
} from "@shopify/polaris";
import { AddNoteMajor, ShareMinor } from "@shopify/polaris-icons";
import { useState } from "react";

import TypeIcon from "./children/TypeIcon";
import PreviewIcon from "./children/PreviewIcon";
import OpenTab from "./children/OpenTab";
import { IIcons, ITab } from "~/interface/buttonSetting";
import { iconsSameTypeCta } from "./config/configCtaButton";
import { iconsTypeSocial } from "./config/configSocialButton";
import { iconsTypeShare } from "./config/configShareButton";
import { radioButton } from "./config/configRadioButton";
import { DisplayEnum, IconTabEnum, TabEnum } from "~/constants/enum";

const RenderTab = ({ checkedTab }: ITab) => {
  const [isCheckedButton, setIsCheckedButton] = useState<string>(
    IconTabEnum.DISABLED
  );
  const [isCheckedDisplay, setIsCheckedDisplay] = useState<string>(
    DisplayEnum.ICON
  );
  const [iconSelected, setIconSelected] = useState<React.FunctionComponent<
    React.SVGProps<SVGSVGElement>
  > | null>(null);
  const [isOpenAddImage, setIsOpenAddImage] = useState<boolean>(false);
  const validImageTypes = ["image/gif", "image/jpeg", "image/png"];
  const [iconsType, setIconType] = useState<IIcons[]>([]);
  const [rangeValue, setRangeValue] = useState(100);
  const [textValue, setTextValue] = useState<string>("");
  const [openTab, setOpenTab] = useState<number>(0);
  const [background, setBackground] = useState("#fff");
  const [color, setColor] = useState<string>("#000");
  const [files, setFiles] = useState<File[]>([]);

  const handleSelectIcon = (
    item: React.FunctionComponent<React.SVGProps<SVGSVGElement>> | null,
    index: number
  ) => {
    if (checkedTab === TabEnum.CTA_BUTTON) {
      setIconSelected(item);
      const iconFilter = iconsSameTypeCta.filter(
        (icons) => icons.label === item
      );
      setIconType(iconFilter);

      setOpenTab(index);
    }
    if (checkedTab === TabEnum.SOCIAL_BUTTON) {
      setIconSelected(item);
      const iconFilter = iconsTypeSocial.filter(
        (icons) => icons.label === item
      );
      setIconType(iconFilter);
      setOpenTab(index);
    }
    if (item === ShareMinor && checkedTab === TabEnum.SHARE_BUTTON) {
      setIconSelected(item);
      const iconFilter = iconsTypeShare.filter((icons) => icons.label === item);
      setIconType(iconFilter);
    }
  };

  const handleCheckedButton = (_: boolean, newValue: string) =>
    setIsCheckedButton(newValue);

  const handleClickButton = () => {
    setIsOpenAddImage(true);
    setIconSelected(null);
  };

  const handleChangeTextTooltip = (newValue: string) => {
    setTextValue(newValue);
  };

  const handleCheckedRadioDisplay = (_: boolean, newValue: string) => {
    if (newValue === DisplayEnum.ICON) {
      return setIsCheckedDisplay(newValue);
    }
    if (newValue === DisplayEnum.ICON_TEXT) {
      return setIsCheckedDisplay(newValue);
    }
    if (newValue === DisplayEnum.TEXT) {
      return setIsCheckedDisplay(newValue);
    }
    if (newValue === DisplayEnum.TEXT_HOVER) {
      return setIsCheckedDisplay(newValue);
    }
  };

  const handleDropZoneDrop = (
    _dropFiles: File[],
    acceptedFiles: File[],
    _rejectedFiles: File[]
  ) => {
    setIsOpenAddImage(false);

    return setFiles(acceptedFiles);
  };

  const fileUpload = !files.length && <DropZone.FileUpload />;
  const uploadedFiles = files.length > 0 && (
    <div style={{ padding: "0", alignItems: "center" }}>
      {files.map((file, index) => (
        <Thumbnail
          key={index}
          size="large"
          alt={file.name}
          source={
            validImageTypes.includes(file.type)
              ? window.URL.createObjectURL(file)
              : AddNoteMajor
          }
        />
      ))}
    </div>
  );

  const handleSelectIconSameType = (
    item: React.FunctionComponent<React.SVGProps<SVGSVGElement>>
  ) => {
    setIconSelected(item);
  };

  const handleRangeSliderChange = (value: number) => setRangeValue(value);

  const handlePickColor = (value: string) => setColor(value);
  const handlePickBgColor = (newValue: string) => setBackground(newValue);

  return (
    <ButtonSettingsStyled
      $BgColor={background}
      $Color={color}
      $RangeValue={rangeValue}
    >
      <Card>
        <div className="cta-icon-buttons">
          <PreviewIcon
            iconSelected={iconSelected}
            isCheckedDisplay={isCheckedDisplay}
            textValue={textValue}
            files={files}
            validImageTypes={validImageTypes}
          />
        </div>
        <CustomText type="header">TYPE</CustomText>
        <div className="cta-icon-type">
          <div className="cta-icon-margin">
            <Grid columns={{ xs: 1, sm: 3, md: 4, lg: 6, xl: 8 }}>
              <TypeIcon
                checkedTab={checkedTab}
                handleSelectIcon={handleSelectIcon}
              />
            </Grid>
          </div>
        </div>
        <Divider borderColor="border" />
        <div className="cta-icon-type">
          <Grid columns={{ xs: 1, sm: 3, md: 4, lg: 4, xl: 4 }}>
            <CustomText type="header">ICON</CustomText>
            <RadioButton
              label="Default icon"
              id={IconTabEnum.DISABLED}
              checked={isCheckedButton === IconTabEnum.DISABLED}
              onChange={handleCheckedButton}
            />
            <RadioButton
              label="Custom icon"
              id={IconTabEnum.CUSTOMER}
              checked={isCheckedButton === IconTabEnum.CUSTOMER}
              onChange={handleCheckedButton}
            />
          </Grid>
        </div>
        <div className="cta-icon-type">
          {isCheckedButton === IconTabEnum.DISABLED ? (
            <>
              {iconsType.map((item, index) => (
                <div style={{ display: "flex" }} key={index}>
                  {item.icons.map(
                    (
                      icon: React.FunctionComponent<
                        React.SVGProps<SVGSVGElement>
                      >,
                      index: number
                    ) => (
                      <div key={index} className="cta-icon-button">
                        <Button
                          fullWidth
                          size="large"
                          icon={icon}
                          onClick={() => handleSelectIconSameType(icon)}
                        />
                      </div>
                    )
                  )}
                </div>
              ))}
            </>
          ) : (
            <>
              {isOpenAddImage ? (
                <div className="button-upload-file">
                  <DropZone onDrop={handleDropZoneDrop}>
                    {uploadedFiles}
                    {fileUpload}
                  </DropZone>
                </div>
              ) : (
                <div>
                  <div className="cta-icon-default"></div>
                  <div className="button-change">
                    <Button fullWidth onClick={handleClickButton}>
                      Change
                    </Button>
                  </div>
                </div>
              )}
            </>
          )}
        </div>
        <Divider borderColor="border" />
        <div className="cta-icon-type">
          <CustomText type="header">ICON COLOR</CustomText>
          <input
            className="cta-input-color"
            type="color"
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              handlePickColor(e.target.value)
            }
          />
          <span className="cta-input-color">||</span>
          <CustomText type="header"> BACKGROUND COLOR:</CustomText>
          <input
            className="cta-input-color"
            type="color"
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              handlePickBgColor(e.target.value)
            }
          />
          <span className="cta-input-color">||</span>
          <CustomText type="header"> BACKGROUND OPACITY:</CustomText>
          <div className="cta-input-color">
            <RangeSlider
              label=""
              value={rangeValue}
              onChange={handleRangeSliderChange}
              output
            />
          </div>
        </div>
        <Divider borderColor="border" />
        <div className="cta-icon-type">
          <Grid columns={{ xs: 1, sm: 3, md: 4, lg: 4, xl: 8 }}>
            <CustomText type="header">DISPLAY:</CustomText>
            {radioButton.map((icon, index) => (
              <RadioButton
                key={index}
                label={icon.label}
                id={icon.id}
                checked={isCheckedDisplay === icon.id}
                onChange={handleCheckedRadioDisplay}
              />
            ))}
          </Grid>
        </div>
        <Divider borderColor="border" />

        <OpenTab openTab={openTab} checkedTab={checkedTab} />

        <div className="cta-icon-type">
          <CustomText type="header">TEXT/TOOLTIP</CustomText>
          <div className="text-field">
            <TextField
              size="medium"
              label=""
              value={textValue}
              onChange={handleChangeTextTooltip}
              maxLength={400}
              autoComplete="off"
              showCharacterCount
            />
          </div>
        </div>
      </Card>
    </ButtonSettingsStyled>
  );
};

export default RenderTab;
