import { Checkbox, Grid } from "@shopify/polaris";
import { memo, useState } from "react";
import TextInputOpenTab from "./TextInputOpenTab";
import RadioButtonOpenTab from "./RadioButtonOpenTab";
import { iconTypeCta } from "../config/configCtaButton";
import { iconSocialButtons } from "../config/configSocialButton";
import { RadioButtEnum, TabEnum, TypeInputEnum } from "~/constants/enum";

interface OpenTabProps {
  openTab: number;
  checkedTab: string;
}

const OpenTab = ({ openTab, checkedTab }: OpenTabProps) => {
  const [inputValue, setInputValue] = useState<string>("");
  const [isRadioTab, setIsRadioTab] = useState<string>(RadioButtEnum.NEW_TAB);
  const [checked, setChecked] = useState<boolean>(false);

  const handleChangeInput = (newValue: string) => {
    setInputValue(newValue);
  };
  const handleCheckOpenTab = (_: boolean, newValue: string) => {
    setIsRadioTab(newValue);
  };

  const handleChange = (newChecked: boolean) => setChecked(newChecked);

  return (
    <div>
      {checkedTab === TabEnum.CTA_BUTTON && (
        <>
          {iconTypeCta.map((item, index) => (
            <div key={index}>
              {openTab === index && (
                <>
                  {item.typeInput === TypeInputEnum.INPUT_BOX && (
                    <TextInputOpenTab
                      textInputProp={item.textInput}
                      inputValue={inputValue}
                      handleChangeInput={handleChangeInput}
                    />
                  )}
                  {item.typeRadio === TypeInputEnum.RADIO_BUTTON && (
                    <RadioButtonOpenTab
                      textRadioProp={item.textRadio}
                      isRadioTab={isRadioTab}
                      handleCheckOpenTab={handleCheckOpenTab}
                    />
                  )}
                </>
              )}
            </div>
          ))}
        </>
      )}
      {checkedTab === TabEnum.SOCIAL_BUTTON && (
        <>
          {iconSocialButtons.map((item, index) => (
            <div key={index}>
              {openTab === index && (
                <>
                  {item.typeInput === TypeInputEnum.INPUT_BOX &&
                  item.typeRadio === TypeInputEnum.RADIO_BUTTON ? (
                    <>
                      <TextInputOpenTab
                        textInputProp={item.textInput}
                        inputValue={inputValue}
                        handleChangeInput={handleChangeInput}
                        placehoder={item.placeholder}
                      />
                      <RadioButtonOpenTab
                        textRadioProp={item.textRadio}
                        isRadioTab={isRadioTab}
                        handleCheckOpenTab={handleCheckOpenTab}
                      />
                    </>
                  ) : (
                    <>
                      {item.typeInput === TypeInputEnum.INPUT_BOX && (
                        <TextInputOpenTab
                          textInputProp={item.textInput}
                          inputValue={inputValue}
                          handleChangeInput={handleChangeInput}
                          placehoder={item.placeholder}
                        />
                      )}
                    </>
                  )}
                  {item.subchilren &&
                    item.subchilren.map((childItem, index) => (
                      <div key={index}>
                        {childItem.typeInput === TypeInputEnum.INPUT_BOX &&
                        childItem.typeRadio === TypeInputEnum.RADIO_BUTTON ? (
                          <>
                            <TextInputOpenTab
                              textInputProp={childItem.textLabel}
                              inputValue={inputValue}
                              handleChangeInput={handleChangeInput}
                              placehoder={childItem.placeholder}
                            />
                            <Grid columns={{ xs: 4, sm: 4, md: 2, xl: 2 }}>
                              <TextInputOpenTab
                                textInputProp={childItem.textInput}
                                inputValue={inputValue}
                                handleChangeInput={handleChangeInput}
                                placehoder={childItem.placeholder}
                              />
                              <div className="text-field-checkbox">
                                <Checkbox
                                  label={childItem.label}
                                  checked={checked}
                                  onChange={handleChange}
                                />
                              </div>
                            </Grid>

                            <RadioButtonOpenTab
                              textRadioProp={childItem.textRadio}
                              isRadioTab={isRadioTab}
                              handleCheckOpenTab={handleCheckOpenTab}
                            />
                          </>
                        ) : (
                          <>
                            <TextInputOpenTab
                              textInputProp={childItem.textLabel}
                              inputValue={inputValue}
                              handleChangeInput={handleChangeInput}
                              placehoder={childItem.placeholder}
                            />
                            <Grid columns={{ xs: 4, sm: 4, md: 2, xl: 2 }}>
                              <TextInputOpenTab
                                textInputProp={childItem.textInput}
                                inputValue={inputValue}
                                handleChangeInput={handleChangeInput}
                                placehoder={childItem.placeholder}
                              />
                              <div className="text-field-checkbox">
                                <Checkbox
                                  label={childItem.label}
                                  checked={checked}
                                  onChange={handleChange}
                                />
                              </div>
                            </Grid>
                          </>
                        )}
                      </div>
                    ))}
                </>
              )}
            </div>
          ))}
        </>
      )}
    </div>
  );
};

export default memo(OpenTab);
