import { Divider, Grid, RadioButton } from "@shopify/polaris";
import { memo } from "react";
import CustomText from "~/components/CustomText/CustomText";
import { RadioButtEnum } from "~/constants/enum";

interface RadioInputProps {
  textRadioProp?: string;
  isRadioTab: string;
  handleCheckOpenTab: (_: boolean, newValue: string) => void;
}

const RadioButtonOpenTab = ({
  textRadioProp,
  isRadioTab,
  handleCheckOpenTab,
}: RadioInputProps) => {
  return (
    <div>
      <div className="cta-icon-type">
        <Grid columns={{ xs: 1, sm: 3, md: 4, lg: 4, xl: 4 }}>
          <CustomText type="header">{textRadioProp}</CustomText>
          <RadioButton
            label="In new tab"
            id={RadioButtEnum.NEW_TAB}
            checked={isRadioTab === RadioButtEnum.NEW_TAB}
            onChange={handleCheckOpenTab}
          />
          <RadioButton
            label="In current tab"
            id={RadioButtEnum.CURRENT_TAB}
            checked={isRadioTab === RadioButtEnum.CURRENT_TAB}
            onChange={handleCheckOpenTab}
          />
        </Grid>
      </div>
      <Divider borderColor="border" />
    </div>
  );
};

export default memo(RadioButtonOpenTab);
