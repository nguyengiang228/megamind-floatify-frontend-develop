import { Divider, TextField } from "@shopify/polaris";
import { memo } from "react";
import CustomText from "~/components/CustomText/CustomText";

interface ITextInput {
  placehoder?: string;
  textInputProp: string;
  inputValue: string;
  handleChangeInput: (newValue: string) => void;
}

const TextInputOpenTab = ({
  placehoder,
  textInputProp,
  inputValue,
  handleChangeInput,
}: ITextInput) => {
  return (
    <div>
      <div className="cta-icon-type">
        <CustomText type="header">{textInputProp}</CustomText>
        <div style={{ flex: 1 }}>
          <div className="text-field-inline">
            <TextField
              label=""
              value={inputValue}
              onChange={handleChangeInput}
              autoComplete="off"
              placeholder={placehoder}
            />
          </div>
        </div>
      </div>
      <Divider borderColor="border" />
    </div>
  );
};
export default memo(TextInputOpenTab);
