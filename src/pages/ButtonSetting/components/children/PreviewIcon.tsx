import { Thumbnail } from "@shopify/polaris";
import { AddNoteMajor } from "@shopify/polaris-icons";
import { memo } from "react";
import { DisplayEnum } from "~/constants/enum";

interface PreviewProps {
  iconSelected: React.FunctionComponent<React.SVGProps<SVGSVGElement>> | null;
  isCheckedDisplay: string;
  textValue: string;
  files: File[];
  validImageTypes: string[];
}

const PreviewIcon = ({
  iconSelected,
  isCheckedDisplay,
  textValue,
  files,
  validImageTypes,
}: PreviewProps) => {
  return (
    <div className="cta-icon-default">
      {iconSelected ? (
        <>
          {isCheckedDisplay === DisplayEnum.ICON && (
            <div className="cta-icon-default">{<>{iconSelected}</>}</div>
          )}
          {isCheckedDisplay === DisplayEnum.ICON_TEXT && (
            <div className="cta-icon-default">
              <div className="cta-icon">{<>{iconSelected}</>}</div>
              {textValue}
            </div>
          )}
          {isCheckedDisplay === DisplayEnum.TEXT && <div>{textValue}</div>}
          {isCheckedDisplay === DisplayEnum.TEXT_HOVER && (
            <div className="cta-icon-default">
              <div className="cta-icon">{<>{iconSelected}</>}</div>
              <div className={textValue.length ? "cta-icon-hover" : ""}>
                {textValue}
              </div>
            </div>
          )}
        </>
      ) : (
        <>
          {files.map((file, index) => (
            <Thumbnail
              key={index}
              source={
                validImageTypes.includes(file.type)
                  ? window.URL.createObjectURL(file)
                  : AddNoteMajor
              }
              alt="Black choker necklace"
            />
          ))}
        </>
      )}
    </div>
  );
};

export default memo(PreviewIcon);
