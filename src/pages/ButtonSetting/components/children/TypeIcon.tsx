import { Button } from "@shopify/polaris";

import { memo } from "react";
import { TabEnum } from "~/constants/enum";
import { iconTypeCta } from "../config/configCtaButton";
import { iconSocialButtons } from "../config/configSocialButton";
import { iconShareButton } from "../config/configShareButton";

interface TypeProps {
  checkedTab: string;
  handleSelectIcon: (
    item: React.FunctionComponent<React.SVGProps<SVGSVGElement>> | null,
    index: number
  ) => void;
}

const TypeIcon = ({ checkedTab, handleSelectIcon }: TypeProps) => {
  return (
    <>
      {checkedTab === TabEnum.CTA_BUTTON && (
        <>
          {iconTypeCta.map((item, index) => (
            <div className="cta-icon-button" key={index}>
              <Button
                fullWidth
                size="large"
                icon={item.label}
                onClick={() => handleSelectIcon(item.label, index)}
              />
            </div>
          ))}
        </>
      )}
      {checkedTab === TabEnum.SOCIAL_BUTTON && (
        <>
          {iconSocialButtons.map((item, index) => (
            <div className="cta-icon-button" key={index}>
              <Button
                fullWidth
                size="large"
                icon={item.label}
                onClick={() => handleSelectIcon(item.label, index)}
              />
            </div>
          ))}
        </>
      )}
      {checkedTab === TabEnum.SHARE_BUTTON && (
        <>
          {iconShareButton.map((item, index) => (
            <div className="cta-icon-button" key={index}>
              <Button
                fullWidth
                size="large"
                icon={item.label}
                onClick={() => handleSelectIcon(item.label, index)}
              />
            </div>
          ))}
        </>
      )}
    </>
  );
};

export default memo(TypeIcon);
