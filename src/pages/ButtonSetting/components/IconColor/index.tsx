import { ColorPicker } from "@shopify/polaris";
import { useState } from "react";

const IconColor = () => {
  const [color, setColor] = useState({
    hue: 120,
    brightness: 1,
    saturation: 1,
  });
  console.log("color:", color);

  return (
    <div>
      <ColorPicker onChange={setColor} color={color} />
    </div>
  );
};

export default IconColor;
