import {
  ArrowUpMinor,
  HomeMinor,
  CardReaderMajor,
  PhoneMajor,
  ViewportNarrowMajor,
  MobileBackArrowMajor,
  HomeMajor,
  HomeFilledMinor,
  CardReaderChipMajor,
  CardReaderTapMajor,
  PhoneInMajor,
  PhoneOutMajor,
} from "@shopify/polaris-icons";
export const iconTypeCta = [
  { label: ArrowUpMinor },
  { label: HomeMinor, textRadio: "OPEN TAB", typeRadio: "radio" },
  { label: CardReaderMajor, textRadio: "OPEN TAB", typeRadio: "radio" },
  {
    label: PhoneMajor,
    textInput: "PHONE NUMBER",
    typeInput: "input",
  },
];

export const iconsSameTypeCta = [
  { label: ArrowUpMinor, icons: [ViewportNarrowMajor, MobileBackArrowMajor] },
  { label: HomeMinor, icons: [HomeMajor, HomeFilledMinor] },
  {
    label: CardReaderMajor,
    icons: [CardReaderChipMajor, CardReaderTapMajor],
  },
  { label: PhoneMajor, icons: [PhoneInMajor, PhoneOutMajor] },
];
