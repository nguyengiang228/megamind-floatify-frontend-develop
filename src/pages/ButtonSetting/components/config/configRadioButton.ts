export const radioButton = [
  { label: "Ony Icon", id: "icon" },
  { label: "Icon + Text", id: "icon_Text" },
  { label: "Ony Text", id: "text" },
  { label: "Display text on hover", id: "text_Hover" },
];
