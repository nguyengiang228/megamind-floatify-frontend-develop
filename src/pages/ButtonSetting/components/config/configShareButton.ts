import {
  ShareMinor,
  FacebookMinor,
  TwitterMinor,
  SnapchatMinor,
  ShareIosMinor,
} from "@shopify/polaris-icons";

export const iconShareButton = [
  { label: ShareMinor, key: "share", typeInput: "", typeRadio: "" },
  { label: FacebookMinor },
  { label: TwitterMinor, key: "twitter" },
  { label: SnapchatMinor, key: "snap" },
];

export const iconsTypeShare = [
  { label: ShareMinor, key: "share", icons: [ShareIosMinor, ShareMinor] },
  { label: FacebookMinor, key: "facebook", icons: [ShareIosMinor, ShareMinor] },
];
