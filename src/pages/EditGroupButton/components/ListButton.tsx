import {
  Banner,
  BlockStack,
  Box,
  Button,
  ButtonGroup,
  Card,
  Icon,
} from "@shopify/polaris";
import CustomText from "~/components/CustomText/CustomText";
import {
  PhoneMajor,
  EditMinor,
  DeleteMinor,
  MobileVerticalDotsMajor,
  // AddMajor,
} from "@shopify/polaris-icons";
import { useNavigate } from "react-router-dom";

const BUTTONS = [
  {
    name: "Whatsapp",
    title: "0123456789",
    icon: PhoneMajor,
  },
  {
    name: "Phone",
    title: "0123456789",
    icon: PhoneMajor,
  },
];

function ListButton() {
  const navigate = useNavigate();
  const handleAddNewButton = () => {
    navigate(`/button-settings`);
  };

  return (
    <Card>
      <CustomText type="header">List buttons</CustomText>
      <Box paddingBlock="400">
        <Banner title="Note">
          The Buttons: Sms, Phone, Viber, Line only work when clicking on them
          on mobile devices!
        </Banner>
      </Box>
      <BlockStack>
        <div className="title-and-new-button">
          <CustomText type="body">Drag and Drop to Rearrange Order</CustomText>
          <div className="new-button" onClick={handleAddNewButton}>
            {/* <Icon source={AddMajor} tone="primary" /> */}
            <div> + New Button</div>
          </div>
          {/* <ButtonGroup>
            <Button icon={AddMajor} onClick={handleAddNewButton}>
              New Button
            </Button>
          </ButtonGroup> */}
        </div>

        <BlockStack>
          {BUTTONS.map((button, index) => (
            <div className="button-list" key={index}>
              <div className="button-move">
                <Icon source={MobileVerticalDotsMajor} tone="subdued" />
              </div>
              <div>
                <Icon source={button.icon} tone="base" />
              </div>
              <div>
                <CustomText type="header">{button.name}</CustomText>
                <CustomText type="sub">{button.title}</CustomText>
              </div>
              <div className="button-actions">
                <ButtonGroup>
                  <Button icon={EditMinor} />
                  <Button icon={DeleteMinor} />
                </ButtonGroup>
              </div>
            </div>
          ))}
        </BlockStack>
      </BlockStack>
    </Card>
  );
}

export default ListButton;
