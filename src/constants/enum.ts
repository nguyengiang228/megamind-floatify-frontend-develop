export const TabEnum = {
  CTA_BUTTON: "cta",
  SOCIAL_BUTTON: "social",
  SHARE_BUTTON: "share",
};

export const TypeInputEnum = {
  RADIO_BUTTON: "radio",
  INPUT_BOX: "input",
};

export const RadioButtEnum = {
  NEW_TAB: "new_tab",
  CURRENT_TAB: "current_tab",
};

export const DisplayEnum = {
  ICON: "icon",
  ICON_TEXT: "icon_Text",
  TEXT: "text",
  TEXT_HOVER: "text_Hover",
};

export const IconTabEnum = {
  DISABLED: "disabled",
  CUSTOMER: "customer",
};
