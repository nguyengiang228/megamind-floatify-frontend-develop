export interface IIcons {
  label: React.FunctionComponent<React.SVGProps<SVGSVGElement>>;
  icons: React.FunctionComponent<React.SVGProps<SVGSVGElement>>[];
}
export interface IIconType {
  typeRadio?: string;
  typeInput?: string;
}

export interface ITab {
  checkedTab: string;
}
